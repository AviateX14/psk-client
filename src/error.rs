use failure::Fail;
use hex::FromHexError;
use openssl::error::ErrorStack;
use std::io;

use openssl_errors::openssl_errors;

/// Simple wrapper around crates own, and dependent crates', error types.
#[derive(Debug, Fail)]
pub enum PskClientError {
    /// Generated when the caller fails supply a PSK key.
    #[fail(display = "Missing PSK Key")]
    MissingKey,
    /// Generated when the supplied PSK key isn't valid HEX.
    #[fail(display = "Could not parse PSK key hex: {}", _0)]
    UnparseableKeyHex(FromHexError),
    /// Could not read from the given `Read` or `BufRead` object
    #[fail(display = "Could not read input: {}", _0)]
    ReadError(io::Error),
    /// Generated when the caller fails to supply a PSK identity.
    #[fail(display = "Missing PSK identity")]
    MissingIdentity,
    /// Generated when the supplied host to connect to is invalid.
    #[fail(display = "The specified host is invalid: {}", _0)]
    NoValidHost(io::Error),
    /// Generated when OpenSSL cannot be initialised, perhaps it's missing?
    #[fail(display = "Failed to initialise OpenSSL: {}", _0)]
    FailedInitialisation(ErrorStack),
    /// Generated when the supplied ciphers do not translate to a valid cipher string.
    #[fail(display = "Invalid cipherlist '{}': {}", _0, _1)]
    InvalidCipherList(String, ErrorStack),
    /// Generated when the client fails to open a basic TCP connection to the host.
    #[fail(display = "TCP stream connection error: {}", _0)]
    TcpConnectError(io::Error),
    /// Generated when the SSL handshake fails.
    #[fail(display = "SSL handshake error: {}", _0)]
    SslHandshakeError(String),
    /// Generated when the FIPS was marked as required but could not be enabled.
    #[fail(display = "FIPS could not be enabled, is OpenSSL compiled with FIPS enabled?")]
    FIPSError,
}

openssl_errors! {
    /// Provides a route to propgate errors up from the `PskClient` into an OpenSSL
    /// type errors stack. This is required in some scenarios, such as PSK negotiation.
    pub library PskClientOpenSSLError("PSK Client Helper Library") {
        functions {
            /// Generated when an error occurs in writing a given identity to the remote socket.
            CONNECT_WRITE_ID("connect_write_identity");

            /// Generated when an error occurs in writing a given PSK to the remote socket.
            CONNECT_WRITE_KEY("connect_write_key");
        }

        reasons {
            /// Represents an error of type `std::io::Error`.
            IO_ERROR("An IO error occured");
        }
    }
}
