use psk_client::{error::PskClientError, PskClient};

fn main() -> Result<(), PskClientError> {
    let client = PskClient::builder("127.0.0.1:4433")
        .reset_ciphers()
        .cipher("PSK-AES128-CBC-SHA")
        .cipher("PSK-AES256-CBC-SHA")
        .identity("Client_identity")
        .key("4836525835726d466c743469426c55356e377375436254566d51476937724932")
        .build()?;

    println!("{:?}", client.connect());

    Ok(())
}
