# PSK Client

This is a simple wrapper around the PSK functionality exposed by the [openssl crate](https://github.com/sfackler/rust-openssl). PR's to make this more generic, useable and informative (in terms of errors) are more than welcome.

## Features

PSK Client has one feature which is `openssl-vendored` which simply enables the vendored feature on the openssl crate, for further information, see the [openssl-rs docs](https://docs.rs/openssl/0.10.19/openssl/#vendored).

## Usage

```rust
use psk_client::{PskClient, error::PskClientError};

fn main() -> Result<(), PskClientError> {
    let client = PskClient::builder("127.0.0.1:4433")
        .reset_ciphers()
        .cipher("PSK-AES128-CBC-SHA")
        .cipher("PSK-AES256-CBC-SHA")
        .identity("Client_identity")
        .key("4836525835726d466c743469426c55356e377375436254566d51476937724932")
        .build()?;

    let mut connection = client.connect()?;

    if let Err(msg) = connection.write_all(b"Hello, World!") {
        eprintln!("Error writing to client: {}", msg);
    }

    Ok(())
}
```

A key may also be retrieved from a file (or anything implementing `Read`), like so:
```rust
use psk_client::{PskClient, error::PskClientError};
use std::fs::File;

fn main() -> Result<(), PskClientError> {
    let key_file = File::open("/some/path/to/psk.key").unwrap();
    
    let client = PskClient::builder("127.0.0.1:4433")
        .identity("Client_identity")
        .key_from(key_file)?
        .build()?;
    
    let mut connection = client.connect()?;

    if let Err(msg) = connection.write_all(b"Hello, World!") {
        eprintln!("Error writing to client: {}", msg);
    }

    Ok(())
}
```

## Default Ciphers
By default the client will use the following ciphers, this can be cleared by calling `reset_ciphers()` on a `PskClientBuilder`. You can supply your own ciphers, either after clearing the pre-defined cipers, or in addition to
them by calling `cipher("<cipher>")` on a `PskClientBuilder` as shown in the first example above.
* `RSA-PSK-AES256-GCM-SHA384`
* `DHE-PSK-AES256-GCM-SHA384`
* `RSA-PSK-CHACHA20-POLY1305`
* `DHE-PSK-CHACHA20-POLY1305`
* `DHE-PSK-AES256-CCM8`
* `DHE-PSK-AES256-CCM`
* `PSK-AES256-GCM-SHA384`
* `PSK-CHACHA20-POLY1305`
* `PSK-AES256-CCM8`
* `PSK-AES256-CCM`
* `RSA-PSK-AES128-GCM-SHA256`
* `DHE-PSK-AES128-GCM-SHA256`
